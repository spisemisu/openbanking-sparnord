Open Banking - Spar Nord
========================

This is just a simple example on how you can go from Business Data to a
`type-safe` Domain Specific Language (`DSL`) where you can easily work on the
data. Mostly this is done for the `LULZ`, but also to show people who follow my
Haskell Intro course, how easy it is.

The project only contains the domain file `Data.Domain` which was more or less
made by `copy/pasting` data from Spar Nords spreadsheet (`true story`). Once the
(`type-safe`) domain is represented as code, we can begin to work with it like
we would do with `C# - LINQ`.


# Spar Nord

## Open data

At Spar Nord we love innovation and we love data - lots of it! It helps us build
better products for our customers! That’s why we are releasing an open dataset
for you to play with and create your own awesome data analysis projects. We have
a lot of smart people working at Spar Nord, but we also realize that we might
not have ALL the smart people working with us and therefore we want to invite
all you creative talented people out there to work your magic on our open data
and who knows, maybe you see some angles or spot some trends that we don’t!

So we strongly encourage you to share your projects with us - whether it is a
fancy machine learning model, a cool visualisation or a funny correlation - we
would love to see what you come up with! Please let us know if you have any
suggestions to future datasets we should consider opening. The first dataset we
are releasing contains all the withdrawals made in Spar Nord ATMs in 2017. The
dataset contains information about the location of the ATM, manufacturer,
currency, cardtype, weather conditions, etc. See our data dictionary for a
complete overview of the variables or join the discussion in our slack
channel. Hit us with your projects and comments at
mailto:openbanking@sparnord.dk

## Resources

https://sparnordopenbanking.com/OpenData/
