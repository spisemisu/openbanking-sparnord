Data
====

The files `atm_data_part1.csv` and `atm_data_part2.csv` aren't submitted to this
`GitLab` due to it's size:

```
total 503M
-rw-r--r-- 1 user user 254M Jan 12 18:56 atm_data_part1.csv
-rw-r--r-- 1 user user 250M Jan 12 18:56 atm_data_part2.csv
-rw-r--r-- 1 user user  559 Jan 12 18:56 atm_data_sample.csv
```

You can easily extract the from the `open_dataset.zip` which can be downloaded
from:

<https://sparnordopenbanking.com/OpenData/>
