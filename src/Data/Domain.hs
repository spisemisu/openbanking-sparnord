{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Data.Domain
  ( AutomatedTellerMachine (..)
  , Currency (..)
  , Error (..)
  , Location (..)
  , Manufacturer (..)
  , Month (..)
  , Status (..)
  , Timestamp (..)
  , Weekday (..)
  , Withdrawal (..)
  , parse
  ) where

--------------------------------------------------------------------------------

import           Data.Char
    ( toLower
    )
import qualified Data.List.Split as Split
import           Data.Word
    ( Word16
    , Word8
    )

--------------------------------------------------------------------------------

-- Just copy paste from `data_dictionary.xlsx` and vupti, you suddently have a
-- Domain Specific Language (DSL) which is type-safe !!!

data Weekday
  = Sunday
  | Monday
  | Tuesday
  | Wednesday
  | Thursday
  | Friday
  | Saturday
  deriving (Eq, Ord, Read, Show)

data Month
  = January
  | February
  | March
  | April
  | May
  | June
  | July
  | August
  | September
  | October
  | November
  | December
  deriving (Eq, Ord, Read, Show)

data Timestamp = TS
  { year    :: Word
  , month   :: Word8
  , day     :: Word8
  , weekday :: Weekday
  , hour    :: Word8
  }
  deriving (Eq, Ord, Show)

data Status
  = Active
  | Inactive
  deriving (Eq, Ord, Read, Show)

data Manufacturer
  = NCR
  | DieboldNixdorf
  deriving (Eq, Ord, Show)

data Location = Location
  { description  :: String
  , streetName   :: String
  , streetNumber :: Word16
  , zipcode      :: Word16
  }
  deriving (Eq, Ord, Show)

data AutomatedTellerMachine = ATM
  { status       :: Status
  , uid          :: Word8
  , manufacturer :: Manufacturer
  -- , location     :: Location
  }
  deriving (Eq, Ord, Show)

data Currency
  = DKK
  | EUR
  | GBP
  | USD
  deriving (Eq, Ord, Read, Show)

data Error = Message
  { code :: String
  , text :: String
  }
  deriving (Eq, Ord, Show)

data Withdrawal = Withdrawal
  { timestamp :: Timestamp
  , atm       :: AutomatedTellerMachine
  , currency  :: Currency
  , card      :: String
  , customer  :: Bool
  --, error     :: Maybe Error
  }
  deriving (Eq, Ord, Show)

--------------------------------------------------------------------------------

instance Enum Month where
  fromEnum = aux
    where
      aux January   = 01
      aux February  = 02
      aux March     = 03
      aux April     = 04
      aux May       = 05
      aux June      = 06
      aux July      = 07
      aux August    = 08
      aux September = 09
      aux October   = 10
      aux November  = 11
      aux December  = 12
  toEnum = aux
    where
      aux 01 = January
      aux 02 = February
      aux 03 = March
      aux 04 = April
      aux 05 = May
      aux 06 = June
      aux 07 = July
      aux 08 = August
      aux 09 = September
      aux 10 = October
      aux 11 = November
      aux 12 = December
      aux __ = error "Enum Month > toEnum > Invalid month."

instance Read Manufacturer where
  readsPrec _ = aux
    where
      aux "NCR"             = [ (NCR           , "") ]
      aux "Diebold Nixdorf" = [ (DieboldNixdorf, "") ]
      aux _________________ = [                      ]

--------------------------------------------------------------------------------

parse :: [ String ] -> [ Withdrawal ]
parse [    ] = []
parse (x:xs) =
  Withdrawal
  { timestamp =
    TS
    { year    = read $ idx !! 00
    , month   =        mth
    , day     = read $ idx !! 02
    , weekday = read $ idx !! 03
    , hour    = read $ idx !! 04
    }
  , atm =
    ATM
    { status       = read $ idx !! 05
    , uid          = read $ idx !! 06
    , manufacturer = read $ idx !! 07
    }
  , currency = read $ idx !! 14
  , card     =        crd !! 00
  , customer = len > 1
  } : parse xs
  where
    idx = Split.splitOn "," x
    mth = fromIntegral . fromEnum $ (read $ idx !! 01 :: Month)
    crd = Split.splitOn " - on-us" $ map toLower $ idx !! 15
    len = length crd
