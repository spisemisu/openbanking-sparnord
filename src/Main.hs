#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --package containers
   --package split
   --
-}

--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Data.Domain
import qualified Data.Map    as Dict

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

groupBy
  :: Ord k
  => (a -> k)
  -> [a]
  -> [(k,[a])]
groupBy f =
  -- https://hoogle.haskell.org/?hoogle=Ord k => (a -> k) -> [a] -> [(k, [a])]
  Dict.toAscList . Dict.fromListWith (++) . map (\a -> (f a, [a]))

logic :: [ Withdrawal ] -> [ String ]
logic =
  map show
  . map (\(g,xs) -> (g, length xs, months xs))
  . groupBy card
  . filter  (\withdrawal -> currency withdrawal == USD)
  where
    months = map fst . groupBy (month . timestamp)

--------------------------------------------------------------------------------

main =
  interact $ unlines . logic . parse . drop 1 . lines
